public class Main {

    public static void main(String[] args) {
        System.out.println("Определить, какой четверти принадлежит точка с координатами (x,y).");
        definitionQuarter(5, 3);
        definitionQuarter(-5, 3);
        definitionQuarter(5, -3);
        definitionQuarter(-5, -3);
        definitionQuarter(0, 0);
    }


    public static void definitionQuarter(int x, int y) {
if (x >0 && y>0)
{
    System.out.println("1 четверть");
}
else if (x<0 && y>0)
{
    System.out.println("2 четверть");
}
else if (x<0 && y<0)
{
    System.out.println("3 четверть");
}
else if (x>0 && y<0)
{
    System.out.println("4 четверть");
}
else {
    System.out.println("Начало координат");
}

    }
}
